import os

with open('bandit.cfg') as f:
    bandit_args = f.read().strip()

command = 'bandit -r . -f json -x "start_bandit.py,hook_bandit.py" --output bandit_out.json'

if bandit_args:
    command = command + ' --skip "' + ','.join(bandit_args.split()) + '"'

print(command)
os.system(command)
