from colorama import Fore, Style, init
import json
import requests
import sys

sys.stdin = open('/dev/tty')

init(convert=True)

bandit_out = requests.get("https://gitlab.com/leshark/test_vuln_CI/-/jobs/artifacts/master/raw/bandit_out.json?job=job1").json()


bandit_config = open('bandit.cfg', 'a')

if len(bandit_out['results']) > 0:
    skip_errors = input(f'You have {len(bandit_out["results"])} vulnerabilities in your code since the last commit. Want to fix them? (y/n): ')
    
    if skip_errors == 'y' or skip_errors == 'Y':
        for result in bandit_out['results']:
            for key in result:
                if key == 'test_id':
                    continue
                print(f'[{Fore.RED}{key.upper()}{Style.RESET_ALL}]', result[key], sep='\n')
            fix_error = input('\nFlag this error as false (or do nothing)? (y/n): ')

            if fix_error == 'y' or fix_error == 'Y':
                bandit_config.write(f' {result["test_id"]}')

bandit_config.close()
